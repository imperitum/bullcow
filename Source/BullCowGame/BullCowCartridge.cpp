// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
//#include "Math/UnrealMathUtility.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();

	SetupAnWordsList();

	//PrintLine(TEXT("Num of valid: %i"), Words.Num());

	InitGame();
}

void UBullCowCartridge::OnInput(const FString& PlayerInput) // When the player hits enter
{
    if (bGameOver)
    {
        ClearScreen();
        InitGame();
    }
    else
    {
		ProcessGuess(PlayerInput);
    }
}

void UBullCowCartridge::InitGame()
{
	PrintLine(TEXT("Welcome!"));

    HiddenWord = Words[FMath::RandRange(0, Words.Num() - 1)];
    Lives = HiddenWord.Len() + 2;
    bGameOver = false;

    PrintLine(TEXT("Hi There!\nGuess the %i letter word!\nYou have %i lives.\nType in your guess and \npress enter to continue..."), HiddenWord.Len(), Lives);
	PrintLine(TEXT("Word: %s"), *HiddenWord);
}

void UBullCowCartridge::EndGame()
{
	bGameOver = true;
	PrintLine(TEXT("\nPress enter to play again."));
}

void UBullCowCartridge::ProcessGuess(FString Guess)
{
	if (HiddenWord == Guess)
	{
		PrintLine(TEXT("\nYou have won!"));
		bGameOver = true;
		EndGame();
		return;
	}

	if (HiddenWord.Len() != Guess.Len())
	{
		PrintLine(TEXT("\nThe hidden word is %i characters long."), HiddenWord.Len());
		PrintLine(TEXT("Try guessing again."));
		return;
	}

	if (!IsIsogram(Guess))
	{
		PrintLine(TEXT("\nNo repeating letters, guess again!"));
		return;
	}

	Lives--;

	if (Lives <= 0)
	{
		ClearScreen();
		PrintLine(TEXT("\nYou have no lives yet!"));
		PrintLine(TEXT("The hidden word was: %s"), *HiddenWord);
		EndGame();
		return;
	}

	FBullCowCount Score = GetBullCows(Guess);

	PrintLine(TEXT("You have %i Bulls and %i Cows."), Score.Bulls, Score.Cows);

	PrintLine(TEXT("\nLost a life!\nYou have %i lives remaining."), Lives);
}

bool UBullCowCartridge::IsIsogram(const FString& Word)
{
	for (int32 Index = 0; Index < Word.Len(); Index++)
	{
		for (int32 Comparison = Index + 1; Comparison < Word.Len(); Comparison++)
		{
			if (Word[Index] == Word[Comparison])
			{
				return false;
			}
		}
	}

	return true;
}

void UBullCowCartridge::SetupAnWordsList()
{	
	const FString WordListPath = FPaths::ProjectContentDir() / TEXT("Word List/HiddenWordList.txt");
	FFileHelper::LoadFileToStringArrayWithPredicate(Words, *WordListPath, [](const FString& Word) 
	{ 
		return Word.Len() >= 4 && Word.Len() <= 8 && IsIsogram(Word);
	});
}

FBullCowCount UBullCowCartridge::GetBullCows(const FString& Guess) const
{
	FBullCowCount Count;

	for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); GuessIndex++)
	{
		if (Guess[GuessIndex] == HiddenWord[GuessIndex])
		{
			Count.Bulls++;
			continue;
		}

		for (int32 HiddenIndex = 0; HiddenIndex < HiddenWord.Len(); HiddenIndex++)
		{
			if (Guess[GuessIndex] == HiddenWord[HiddenIndex])
			{
				Count.Cows++;
				break;
			}
		}
	}

	return Count;
}
